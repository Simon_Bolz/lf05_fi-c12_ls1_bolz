import java.util.Scanner;

public class Addieren {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
//		
//		System.out.println("Bitte eine Zahl eingeben.");
//		int zahl1 = scn.nextInt();
//		
//		System.out.println("Bitte eine zweite Zahl eingeben.");
//		int zahl2 = scn.nextInt();
		
		//int sum = zahl1 + zahl2;
		//System.out.println("Summe: " + sum);
		
		int zahl1 = getUserInput("Erste Zahl: ");
		int zahl2 = getUserInput("Zweite Zahl: ");
		
		int sum = addiere(zahl1, zahl2);
		gebeSummeAus(sum);
		
		//scn.close();
				
	}
	
	public static void gebeSummeAus(int zahl) {
		System.out.println("Summe: " + zahl);
	}
	
	public static int addiere(int zahl_01, int zahl_02) {
		int summe = zahl_01 + zahl_02;
		return summe;
	}
	
	public static int getUserInput(String text) {
		Scanner scn = new Scanner(System.in);
		System.out.println(text);
		int zahl = scn.nextInt();		
		return zahl;
		
	}

}
