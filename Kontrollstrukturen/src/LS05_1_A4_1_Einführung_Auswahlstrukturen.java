import java.util.Scanner;

public class LS05_1_A4_1_Einf�hrung_Auswahlstrukturen {

//AB Auswahlstrukturen
// Aufgabe 1
//	1) Wenn W�schekorb voll, dann stelle Waschmaschine an
//	2) Wenn dreckiges Geschirr vorhanden, dann wasche ab
	// Aufgabe 1.2		

	public static void main(String[] args) {
		aufgabe1ZweiZahlen();
		
		aufgabe1DreiZahlen();
		
		gr��teZahlAusgeben();
	}
	
	
	
	public static void aufgabe1ZweiZahlen() {
		
		System.out.println("Bitte eine ganze Zahl eingeben.");
		Scanner scn = new Scanner(System.in);
		int x = scn.nextInt();
		System.out.println("Bitte eine weitere ganze Zahl eingeben.");
		int y = scn.nextInt();
		//Beide Zahlen gleich gro�
		if (x == y) {
			System.out.println("Beide Zahlen sind gleich.");				
		}
		// Zweite Zahl gr��er als erste Zahl
		else if (x < y) {
			System.out.println("Die zweite Zahl ist gr��er als die erste");
		
		}
		else if (x >= y) {
			System.out.println("Die erste Zahl ist gr��er oder gleich als die 2. Zahl.");
		} else  {
			System.out.println("Die if-Bedingungen sind nicht erf�llt.");
		}		
	}
	
	public static void aufgabe1DreiZahlen() {
		Scanner scn = new Scanner(System.in);
		System.out.println("Bitte die erste Zahl eingeben.");
		int x = scn.nextInt();
		System.out.println("Bitte die zweite Zahl eingeben.");
		int y = scn.nextInt();
		System.out.print("Bitte die erste Zahl eingeben.");
		int z = scn.nextInt();
		
		if (x > y && x > z) {
			System.out.print("Die erste Zahl ist gr��er als die zweite und dritte Zahl.");
		} else if(z > y || z > x) {
			System.out.print("Die dritte Zahl ist gr��er als die zweite Zahl oder die erste Zahl.");
					
		}
		
		
	}
	
	public static void gr��teZahlAusgeben() {
		Scanner scn = new Scanner(System.in);
		System.out.println("Bitte die erste Zahl eingeben.");
		int x = scn.nextInt();
		System.out.println("Bitte die zweite Zahl eingeben.");
		int y = scn.nextInt();
		System.out.print("Bitte die erste Zahl eingeben.");
		int z = scn.nextInt();
		
		if (x > y && x > z) {
			System.out.print("Die erste Zahl ist am gr��ten.");
		} else if (y > x && y > z) {
			System.out.print("Die zweite Zahl ist am gr��ten.");
		} else if (z > x && z > y) {
			System.out.print("Die dritte Zahl ist am gr��ten.");
		}else {
			System.out.print("Keine der Zahlen ist am gr��ten.");
		}
	
	}

}
