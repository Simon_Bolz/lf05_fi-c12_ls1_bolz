﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {        
       
       double zuZahlenderBetrag = fahrkartenbestellungErfassen();      

       // Geldeinwurf
       // -----------       
       double rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
       
       
       // Fahrscheinausgabe
       // -----------------
       
       fahrkartenAusgeben();
       
       // Rückgeldberechnung und -Ausgabe
       // -------------------------------

       rueckgeldAusgeben(rückgabebetrag);
       
       
    }


	private static void round(double zuZahlenderBetrag, int i) {
		// TODO Auto-generated method stub
		
	}
//Methode fahrkartenbestellungErfassen -> kein Übergabeparameter, liefert als Ergebnis zuZahlendenBetrag als double zurück; fragt nach Anzahl der Fahrscheine und dem zu zahlenden Betrag
	public static double fahrkartenbestellungErfassen() {
		 Scanner tastatur = new Scanner(System.in);
		 double anzahlTickets; //Datentyp byte für Anzahl der Tickets gewählt, da dies max. 127 Tickets umfassen kann, was für die Anzahl der Tickets ausreichend sein sollte.
		 double zu_Zahlender_Betrag; 
		 
	     System.out.print("Zu zahlender Betrag (EURO): ");
	     zu_Zahlender_Betrag = tastatur.nextDouble();
	       
	     System.out.print("Anzahl der Tickets: ");
	     anzahlTickets = tastatur.nextDouble();
	     
	     zu_Zahlender_Betrag = zu_Zahlender_Betrag * anzahlTickets;
	     return zu_Zahlender_Betrag;
	}
	
// Methode fahrkartenBezahlen -> Zu zahlender Betrag als Übergabeparameter; liefert Rückgabebetrag als Ergebnis zurück
	public static double fahrkartenBezahlen(double betragZuZahlen) {
		double eingezahlterGesamtbetrag;
		double eingeworfeneMünze;
		Scanner tastatur = new Scanner(System.in);
		eingezahlterGesamtbetrag = 0.00;
		while(eingezahlterGesamtbetrag < betragZuZahlen)
	       {
	    	   double bilanz = betragZuZahlen - eingezahlterGesamtbetrag;
	    	   System.out.printf("Noch zu zahlen: %.2f Euro \n", bilanz);
	    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
	    	   eingeworfeneMünze = tastatur.nextDouble();
	           eingezahlterGesamtbetrag += eingeworfeneMünze;
	       }
		double betragZurRückgabe = (double)eingezahlterGesamtbetrag - (double)betragZuZahlen;
		return betragZurRückgabe;
	}
//Methode um Fahrkarten auszugeben	
	public static void fahrkartenAusgeben() {
		System.out.print("\n Fahrschein(e) wird/werden ausgegeben.");
	       
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	       }
	    System.out.println("\n\n");
	}
//Methode um Rückgeld auszugeben	
	public static void rueckgeldAusgeben(double betragZurück) { 
		if(betragZurück > 0.0)
	       {
	    	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO", betragZurück );
	    	   System.out.println(" wird in folgenden Münzen ausgezahlt:");

	           while(betragZurück >= 2.0) // 2 EURO-Münzen
	           {
	        	  System.out.println("2 EURO");
	        	  betragZurück -= 2.0;
	           }
	           while(betragZurück >= 1.0) // 1 EURO-Münzen
	           {
	        	  System.out.println("1 EURO");
	        	  betragZurück -= 1.0;
	           }
	           while(betragZurück >= 0.5) // 50 CENT-Münzen
	           {
	        	  System.out.println("50 CENT");
	        	  betragZurück -= 0.5;
	           }
	           while(betragZurück >= 0.2) // 20 CENT-Münzen
	           {
	        	  System.out.println("20 CENT");
	        	  betragZurück -= 0.2;
	           }
	           while(betragZurück >= 0.1) // 10 CENT-Münzen
	           {
	        	  System.out.println("10 CENT");
	        	  betragZurück -= 0.1;
	           }
	           while(betragZurück >= 0.05)// 5 CENT-Münzen
	           {
	        	  System.out.println("5 CENT");
	        	  betragZurück -= 0.05;
	           }
	       
	       
	           System.out.println("\nVergessen Sie nicht, die Fahrscheine\n"+    
	        	"vor Fahrtantritt entwerten zu lassen!\n"+
	        	"Wir wünschen Ihnen eine gute Fahrt.");  
	    }
	}
    
}